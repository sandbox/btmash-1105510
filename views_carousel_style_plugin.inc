<?php

// $Id$

/**
 * @file views_carousel_style_plugin.inc
 */

class views_carousel_style_plugin extends views_plugin_style {
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }
}
