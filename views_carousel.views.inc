<?php

// $Id$

/**
 * @file
 *  Provides the views plugin information.
 */

/**
  * Implementation of hook_views_plugin().
  */
function views_carousel_views_plugins() {
  return array(
    'module' => 'views_carousel',
    'style' => array(
      'views_carousel' => array(
        'title' => t('Views Carousel Plugin'),
        'theme' => 'views_carousel_view',
        'help' => t('Display rows in a Carousel.'),
        'handler' => 'views_carousel_style_plugin',
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}
