<?php
// $Id

/**
 * @file
 * Default views template for displaying a carousel
 *
 * - $view: The View object.
 * - $options: Settings for the active style.
 * - $rows: The rows output from the View.
 * - $title: The title of this group of rows. May be empty.
 *
 * @ingroup views_templates
 */
?>
<div class='views-carousel-container'>
  <div class='views-carousel-items-container'>
    <?php foreach($rows as $key => $row): ?>
      <div class='views-carousel-item-container<?php if ($key == 0) { print ' active'; } ?>'><?php print $row; ?></div>
    <?php endforeach; ?>
  </div>
  <?php if (count($rows) > 1): ?>
    <div class='views-carousel-item-controls'>
      <div class='views-carousel-controller-left views-carousel-controller'> <img src="<?php print url(drupal_get_path('module', 'views_carousel'). '/left.png', array('absolute' => TRUE)); ?>"/> <div class="control-text"> Prev </div> </div>
      <div class='views-carousel-controller-right views-carousel-controller'> <div class="control-text"> Next </div> <img src="<?php print url(drupal_get_path('module', 'views_carousel'). '/right.png', array('absolute' => TRUE)); ?>"/> </div>
     
    </div>
  <?php endif; ?>
</div>
